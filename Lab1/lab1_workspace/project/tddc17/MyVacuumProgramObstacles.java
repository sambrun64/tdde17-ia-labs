package tddc17;

import aima.core.environment.liuvacuum.*;
import aima.core.agent.Action;
import aima.core.agent.AgentProgram;
import aima.core.agent.Percept;
import aima.core.agent.impl.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import java.util.Random;

/* Define a couple of int and the equals fonctions */
class Couple{
	public int x;
	public int y;
	
	public Couple(int gx,int gy){
		x=gx;
		y=gy;
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Couple) && (((Couple) o).x == x) && (((Couple) o).y == y);
	}
}

/* Define a Node */
// In this program we represent the map as a graph
// Each Nodes as a Parent witch the node is it linked to
// the path made by the vaccum
// Each Nodes as a Parent Direction
// Each node as a cost witch is the distance between the vaccum and the Node
class Node {
	public int x,y;
	public int cost;
	public Node parent;
	public int dirParent;
	
	
	public Node(int xMap,int yMap, int cost) {
		x = xMap;
		y = yMap;
		this.cost = cost;
	}
	
	@Override
	public boolean equals(Object n) {
		return (n instanceof Node) && ((Node) n).x == x && ((Node) n).y == y;
	}
}

/* allows nodes comparaison, we assume that :
 * node1 < node2 <=> node1.cost < node2.cost */
class NodeComparator implements Comparator<Node>{
	@Override
	public int compare(Node o1, Node o2) {
		if(o1.cost < o2.cost) return -1;
		else if (o1.cost == o2.cost) return 0;
		else return 1;
	}
	
}

class MyAgentStateObstacles {
	/* World */
	public static final int MAX_SIZE = 30; //variable for memory world size representation
	public int[][] world = new int[MAX_SIZE][MAX_SIZE];
	public int initialized = 0;
	
	/* World content */
	final int UNKNOWN = 0;
	final int WALL = 1;
	final int CLEAR = 2;
	final int DIRT = 3;
	final int HOME = 4;
	
	/* Action world */
	final int ACTION_NONE = 0;
	final int ACTION_MOVE_FORWARD = 1;
	final int ACTION_TURN_RIGHT = 2;
	final int ACTION_TURN_LEFT = 3;
	final int ACTION_SUCK = 4;

	/* Agent informations */
	public int agent_x_position = 1;
	public int agent_y_position = 1;
	public int agent_last_action = ACTION_NONE;
	
	/* Home map position */
	public int homeX=-1,homeY=-1; // initialized to -1 and discover during the cleanning 

	/* Directions */
	public static final int NORTH = 0;
	public static final int EAST = 1;
	public static final int SOUTH = 2;
	public static final int WEST = 3;
	public int agent_direction = EAST; // Initial Direction of the Agent
	
	public LinkedList<Integer> currentActions = new LinkedList<Integer>(); // The list that represent all the actions that will be done (currently computed) 
	public LinkedList<Couple> toVisit = new LinkedList<Couple>(); // The list that represent all the positions that are to be visited
	public LinkedList<Integer> directionList = new LinkedList<Integer>(); // The list that represent all the direction for the currentActions array
		// ex : directionList <- [N,E,E,S] means that the bot have to go Nord, East, East and south to reach is current objective.
		// The CurentAction list will contain all action to go Nord, process thus actions (eg : turnLeft,goForward), and do the same with the others elements in directionList
	
	MyAgentStateObstacles() {
		for (int i = 0; i < world.length; i++)
			for (int j = 0; j < world[i].length; j++)
				world[i][j] = UNKNOWN;
		world[1][1] = HOME;
		agent_last_action = ACTION_NONE;
	}

	// Based on the last action and the received percept updates the x & y agent
	// position
	public void updatePosition(DynamicPercept p) {
		Boolean bump = (Boolean) p.getAttribute("bump");

		if (agent_last_action == ACTION_MOVE_FORWARD && !bump) {
			switch (agent_direction) {
			case MyAgentStateObstacles.NORTH:
				agent_y_position--;
				break;
			case MyAgentStateObstacles.EAST:
				agent_x_position++;
				break;
			case MyAgentStateObstacles.SOUTH:
				agent_y_position++;
				break;
			case MyAgentStateObstacles.WEST:
				agent_x_position--;
				break;
			}
		}
	}

	// Function that update the wold map
	public void updateWorld(int x_position, int y_position, int info) {
		world[x_position][y_position] = info;
	}

	// Fonction that print the world map in the console
	public void printWorldDebug() {
		for (int i = 0; i < world.length; i++) {
			for (int j = 0; j < world[i].length; j++) {
				if (world[j][i] == UNKNOWN)
					System.out.print(" ? ");
				if (world[j][i] == WALL)
					System.out.print(" # ");
				if (world[j][i] == CLEAR)
					System.out.print(" . ");
				if (world[j][i] == DIRT)
					System.out.print(" D ");
				if (world[j][i] == HOME)
					System.out.print(" H ");
			}
			System.out.println("");
		}
	}
	
	// Return the coordonate in front of the bot, wherever his orientation
	private Couple frontRobot() {
		switch(agent_direction) {
		case NORTH:
			return new Couple(agent_x_position,agent_y_position - 1);
		case EAST:
			return new Couple(agent_x_position + 1,agent_y_position);
		case SOUTH:
			return new Couple(agent_x_position,agent_y_position + 1);
		case WEST:
			return new Couple(agent_x_position - 1,agent_y_position);
		}
		System.out.println("Should never happen : front !");
		return null;
	}
	
	// Return the coordonate on the left of the bot, wherever his orientation
	private Couple leftRobot() {
		switch(agent_direction) {
		case NORTH:
			return new Couple(agent_x_position - 1,agent_y_position);
		case EAST:
			return new Couple(agent_x_position,agent_y_position - 1);
		case SOUTH:
			return new Couple(agent_x_position + 1,agent_y_position);
		case WEST:
			return new Couple(agent_x_position,agent_y_position + 1);
		}
		System.out.println("Should never happen : left !");
		return null;
	}
	
	// Return the coordonate behind the bot, wherever his orientation
	private Couple backRobot() {
		switch(agent_direction) {
		case NORTH:
			return new Couple(agent_x_position,agent_y_position + 1);
		case EAST:
			return new Couple(agent_x_position - 1,agent_y_position);
		case SOUTH: 
			return new Couple(agent_x_position,agent_y_position - 1);
		case WEST:
			return new Couple(agent_x_position + 1,agent_y_position);
		}
		System.out.println("Should never happen : back !");
		return null;
	}
	
	// Return the coordonate on the right of the bot, wherever his orientation
	private Couple rightRobot() {
		switch(agent_direction) {
		case NORTH:
			return new Couple(agent_x_position + 1,agent_y_position);
		case EAST:
			return new Couple(agent_x_position,agent_y_position + 1);
		case SOUTH:
			return new Couple(agent_x_position - 1,agent_y_position);
		case WEST:
			return new Couple(agent_x_position,agent_y_position - 1);
		}
		System.out.println("Should never happen : right !");
		return null;
	}
	
	/* Function that enable the robot to go north (by adding in the currentActions all the move necessary from each direction) */
	public void gotoNorth() {
		switch(agent_direction) {
		case NORTH:
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case EAST:
			currentActions.addLast(ACTION_TURN_LEFT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case SOUTH:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case WEST:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		}
		System.out.println("Should never happen : gotonorth !");
	}
	
	/* Function that enable the robot to go east (by adding in the currentActions all the move necessary from each direction) */
	public void gotoEast() {
		switch(agent_direction) {
		case NORTH:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case EAST:
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case SOUTH:
			currentActions.addLast(ACTION_TURN_LEFT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case WEST:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		}
		System.out.println("Should never happen : gotoeast !");
	}
	
	/* Function that enable the robot to go south (by adding in the currentActions all the move necessary from each direction) */
	public void gotoSouth() {
		switch(agent_direction) {
		case NORTH:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case EAST:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case SOUTH:
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case WEST:
			currentActions.addLast(ACTION_TURN_LEFT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		}
		System.out.println("Should never happen : gotosouth !");
	}
	
	/* Function that enable the robot to go west (by adding in the currentActions all the move necessary from each direction) */
	public void gotoWest() {
		switch(agent_direction) {
		case NORTH:
			currentActions.addLast(ACTION_TURN_LEFT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case EAST:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case SOUTH:
			currentActions.addLast(ACTION_TURN_RIGHT);
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		case WEST:
			currentActions.addLast(ACTION_MOVE_FORWARD);
			return;
		}
		System.out.println("Should never happen : gotowest !");
	}

	/* Add to the frontier the node in postion x,y, paying attention to maintain a correct order */
	private void addNeighborhood(PriorityQueue<Node>frontier, int x,int y,Node parent,int dirParent) {
		// Create the node
		Node newNode = new Node(x,y,parent.cost+1);
		newNode.parent = parent;
		newNode.dirParent = dirParent; //from parent, go dir
		
		// Check if the node is already in the queue
		Iterator<Node> it = frontier.iterator();
		Node n;
		// to avoid delete and iterate on the same list : 
		ArrayList<Node> toDelete = new ArrayList<Node>();
		int eqNum = 0; // aims to ensure everything is OK
		while(it.hasNext()) {
			n = it.next();
			if(n.equals(newNode)) { // if the node is already in the frontier 
				eqNum++; 
				if(n.cost > newNode.cost) { // if the cost is higher, we find a shortest way
					toDelete.add(n); // Must not pass more than once
					eqNum--;
				}
			}
		}
		// deleting loop
		int c = 0;
		for(Node nodeDel : toDelete) {
			frontier.remove(nodeDel);
			c++;
		}
		// checking the consistency of the list
		if(c > 1) {
			System.out.println("Should never happen : addNeighborhood " + c + " node del !");
		}
		// Adding the node, only if it is not yet in the frontier
		if(eqNum == 0) { // Never see or lower cost
			frontier.add(newNode);
		}else if(eqNum < 0 || eqNum > 1) {// checking the consistency of the list
			System.out.println("Should never happen : addNeighborhood " + eqNum + " !");
			System.exit(1);
		}
	}

	/* return true if the home is on x,y position and if we never saw it */
	private boolean unknowHome(int x,int y) {
		return world[x][y] == HOME && homeX==-1 && homeY==-1;
	}
	
	/* Function that uses the Dijkstra like Algorithm to find the way from the vaccum to the Objective */
	private void findAWay(Couple objective) {
		// Node list, with node cost as priority
		PriorityQueue<Node> nodeQueue = new PriorityQueue<Node>(new NodeComparator());
		//first node
		Node start = new Node(agent_x_position,agent_y_position,0);
		start.dirParent = -1;
		start.parent = null;
		nodeQueue.add(start);
		
		// list of all visited nodes
		ArrayList<Couple> visited = new ArrayList<Couple>();
		
		Node n,newNode;
		while(!nodeQueue.isEmpty()) {
			n = nodeQueue.poll(); // pick the minimum cost node in the list
			visited.add(new Couple(n.x,n.y));
			
			// if the objective is reached
			if(n.x == objective.x && n.y == objective.y) {
				Node pathNode = n;
				
				// if we found the objective we go through all the Nodes
				// until we find the root
				while(pathNode.parent != null) {
					directionList.addFirst(pathNode.dirParent); // add the directions
					pathNode = pathNode.parent;
				}
				return;
			}
			
			if(world[n.x][n.y] == UNKNOWN || unknowHome(n.x,n.y)) {
				// No more neighbors (we cant see beyond an unknown node)
			}else {
				// For all cardinal directions x,y next to the current node,
				// if the x,y position is valid : not a wall, in the array limits and not already visited
				if(n.x - 1 >= 0 && world[n.x-1][n.y] != WALL && !visited.contains(new Couple(n.x-1,n.y))) {//WEST POINT
					addNeighborhood(nodeQueue, n.x-1, n.y, n, WEST); // add the point
				}
				if(n.x + 1 < MAX_SIZE && world[n.x+1][n.y] != WALL && !visited.contains(new Couple(n.x+1,n.y))) {//EAST POINT
					addNeighborhood(nodeQueue, n.x+1, n.y, n, EAST);
				}
				if(n.y + 1 < MAX_SIZE && world[n.x][n.y+1] != WALL && !visited.contains(new Couple(n.x,n.y+1))) {//SOUTH POINT
					addNeighborhood(nodeQueue, n.x, n.y+1, n, SOUTH);
				}
				if(n.y - 1 >= 0 && world[n.x][n.y-1] != WALL && !visited.contains(new Couple(n.x,n.y-1))) {//NORTH POINT
					addNeighborhood(nodeQueue, n.x, n.y-1, n, NORTH);
				}
			}
			
		}
		
		
	}
	
	// Visit the current position = generates actions for the bot
	public void visite(Percept percept) {
		DynamicPercept p = (DynamicPercept) percept;
		Boolean bump = (Boolean) p.getAttribute("bump");
		Boolean dirt = (Boolean) p.getAttribute("dirt");
		Boolean home = (Boolean) p.getAttribute("home");
		
		// if there is a dirt we add the suck action on top of the priority
		if(dirt) {
			currentActions.addLast(ACTION_SUCK);
			return;
		}
		
		// We set the home position if we find it
		// It would be used if the home were randomly generated
		if(home) {
			homeX = agent_x_position;
			homeY = agent_y_position;
		}
		
		// For each direction
		Couple[] robotTab = new Couple[4];
		robotTab[0] = rightRobot();
		robotTab[1] = backRobot();
		robotTab[2] = leftRobot();
		robotTab[3] = frontRobot();
		for(Couple tmp:robotTab) {
			if(!toVisit.contains(tmp)){ // Right
				// if we have to visit the position
				if(world[tmp.x][tmp.y] == UNKNOWN || unknowHome(tmp.x,tmp.y)) {
					toVisit.addLast(tmp);
				}
			}else { // if it contains, as we want a deep first deplacements, we have to remove the node and re-add it
				toVisit.remove(tmp);
				toVisit.addLast(tmp);
			}
		}
		
		//All accessible postion has been reached, go back home
		if(toVisit.isEmpty()) {
			findAWay(new Couple(homeX,homeY));
			return;
		}
		
		// take the nearest ( by deepth first search) position and do to it 
		Couple objective = toVisit.pollLast();
		findAWay(objective);
	}
}

public class MyVacuumProgramObstacles implements AgentProgram {
	private int initnialRandomActions = 10;
	private Random random_generator = new Random();

	// Here you can define your variables!
	public int iterationCounter = 5000;
	public MyAgentStateObstacles state = new MyAgentStateObstacles();

	// moves the Agent to a random start position
	// uses percepts to update the Agent position - only the position, other
	// percepts are ignored
	// returns a random action
	private Action moveToRandomStartPosition(DynamicPercept percept) {
		int action = random_generator.nextInt(6);
		initnialRandomActions--;
		state.updatePosition(percept);
		if (action == 0) {
			state.agent_direction = ((state.agent_direction - 1) % 4);
			if (state.agent_direction < 0)
				state.agent_direction += 4;
			state.agent_last_action = state.ACTION_TURN_LEFT;
			return LIUVacuumEnvironment.ACTION_TURN_LEFT;
		} else if (action == 1) {
			state.agent_direction = ((state.agent_direction + 1) % 4);
			state.agent_last_action = state.ACTION_TURN_RIGHT;
			return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		}
		state.agent_last_action = state.ACTION_MOVE_FORWARD;
		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}

	private Action turnLeft() {
		state.agent_last_action = state.ACTION_TURN_LEFT;
		state.agent_direction = (state.agent_direction - 1 < 0)?3:state.agent_direction - 1;
		return LIUVacuumEnvironment.ACTION_TURN_LEFT;
	}

	private Action turnRight() {
		state.agent_last_action = state.ACTION_TURN_RIGHT;
		state.agent_direction = ((state.agent_direction + 1) % 4);
		return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
	}

	private Action moveForward() {
		state.agent_last_action = state.ACTION_MOVE_FORWARD;
		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}

	private Action suck() {
		state.agent_last_action = state.ACTION_SUCK;
		return LIUVacuumEnvironment.ACTION_SUCK;
	}

	// return the current action
	private Action processAction(int action) {
		if(action == state.ACTION_MOVE_FORWARD) {
			return moveForward();
		}else if(action == state.ACTION_SUCK) {
			return suck();
		}else if(action == state.ACTION_TURN_LEFT) {
			return turnLeft();
		}else if(action == state.ACTION_TURN_RIGHT) {
			return turnRight();
		}else if(action == state.ACTION_NONE){
			return NoOpAction.NO_OP;
		}else {
			System.out.println("Should never happen ! no more action before");
			return NoOpAction.NO_OP;
		}
	}
	
	// fill the action List with the current direction
	private void processDirection(int dir) {
		switch(dir) {
		case MyAgentStateObstacles.SOUTH:
			state.gotoSouth();
			break;
		case MyAgentStateObstacles.NORTH:
			state.gotoNorth();
			break;
		case MyAgentStateObstacles.EAST:
			state.gotoEast();
			break;
		case MyAgentStateObstacles.WEST:
			state.gotoWest();
			break;
		}
	}
	
	@Override
	public Action execute(Percept percept) {

		// DO NOT REMOVE this if condition!!!
		if (initnialRandomActions > 0) {
			return moveToRandomStartPosition((DynamicPercept) percept);
		} else if (initnialRandomActions == 0) {
			// process percept for the last step of the initial random actions
			initnialRandomActions--;
			state.updatePosition((DynamicPercept) percept);
			System.out.println("Processing percepts after the last execution of moveToRandomStartPosition()");
			state.agent_last_action = state.ACTION_SUCK;
			return LIUVacuumEnvironment.ACTION_SUCK;
		}

		// Start here
		System.out.println("x=" + state.agent_x_position);
		System.out.println("y=" + state.agent_y_position);
		System.out.println("dir=" + state.agent_direction);
		
		DynamicPercept p = (DynamicPercept) percept;
		Boolean bump = (Boolean) p.getAttribute("bump");
		Boolean dirt = (Boolean) p.getAttribute("dirt");
		Boolean home = (Boolean) p.getAttribute("home");
		System.out.println("percept: " + p);
		
		// avoid infinite loop
		iterationCounter--;
		if (iterationCounter == 0)
			return NoOpAction.NO_OP;

		// State update based on the percept value and the last action
		state.updatePosition((DynamicPercept) percept);
		if (bump) {
			switch (state.agent_direction) {
			case MyAgentStateObstacles.NORTH:	
				state.updateWorld(state.agent_x_position, state.agent_y_position - 1, state.WALL);
				break;
			case MyAgentStateObstacles.EAST:
				state.updateWorld(state.agent_x_position + 1, state.agent_y_position, state.WALL);
				break;
			case MyAgentStateObstacles.SOUTH:
				state.updateWorld(state.agent_x_position, state.agent_y_position + 1, state.WALL);
				break;
			case MyAgentStateObstacles.WEST:
				state.updateWorld(state.agent_x_position - 1, state.agent_y_position, state.WALL);
				break;
			}
		}
		if (dirt)
			state.updateWorld(state.agent_x_position, state.agent_y_position, state.DIRT);
		else
			state.updateWorld(state.agent_x_position, state.agent_y_position, state.CLEAR);

		//displaying world
		state.printWorldDebug();
		
		// ACTIONS FROM THE PREVIOUS DECISION
		
		// No more actions, we have to compute some news
		if(state.currentActions.isEmpty() && state.directionList.isEmpty()) {
			state.visite(percept);
		}
		
		int action;
		if(!state.currentActions.isEmpty()) { // If we have actions left to do we do them
			return processAction(state.currentActions.pollFirst());
			
		}else {
			if(!state.directionList.isEmpty()) { // If we have direction left we change direction
				processDirection(state.directionList.pollFirst());
				return processAction(state.currentActions.pollFirst());
			}
			
		}
		
		return NoOpAction.NO_OP;
		
	}
}
