package tddc17;

import aima.core.environment.liuvacuum.*;
import aima.core.agent.Action;
import aima.core.agent.AgentProgram;
import aima.core.agent.Percept;
import aima.core.agent.impl.*;

import java.util.Random;

class MyAgentState {
	public int[][] world = new int[30][30];
	public int initialized = 0;
	final int UNKNOWN = 0;
	final int WALL = 1;
	final int CLEAR = 2;
	final int DIRT = 3;
	final int HOME = 4;
	final int ACTION_NONE = 0;
	final int ACTION_MOVE_FORWARD = 1;
	final int ACTION_TURN_RIGHT = 2;
	final int ACTION_TURN_LEFT = 3;
	final int ACTION_SUCK = 4;

	public int agent_x_position = 1;
	public int agent_y_position = 1;
	public int agent_last_action = ACTION_NONE;

	public static final int NORTH = 0;
	public static final int EAST = 1;
	public static final int SOUTH = 2;
	public static final int WEST = 3;
	public int agent_direction = EAST;

	MyAgentState() {
		for (int i = 0; i < world.length; i++)
			for (int j = 0; j < world[i].length; j++)
				world[i][j] = UNKNOWN;
		world[1][1] = HOME;
		agent_last_action = ACTION_NONE;
	}

	// Based on the last action and the received percept updates the x & y agent
	// position
	public void updatePosition(DynamicPercept p) {
		Boolean bump = (Boolean) p.getAttribute("bump");

		if (agent_last_action == ACTION_MOVE_FORWARD && !bump) {
			switch (agent_direction) {
			case MyAgentState.NORTH:
				agent_y_position--;
				break;
			case MyAgentState.EAST:
				agent_x_position++;
				break;
			case MyAgentState.SOUTH:
				agent_y_position++;
				break;
			case MyAgentState.WEST:
				agent_x_position--;
				break;
			}
		}

	}

	public void updateWorld(int x_position, int y_position, int info) {
		if(world[x_position][y_position] == HOME) return;
		world[x_position][y_position] = info;
	}

	public void printWorldDebug() {
		for (int i = 0; i < world.length; i++) {
			for (int j = 0; j < world[i].length; j++) {
				if (world[j][i] == UNKNOWN)
					System.out.print(" ? ");
				if (world[j][i] == WALL)
					System.out.print(" # ");
				if (world[j][i] == CLEAR)
					System.out.print(" . ");
				if (world[j][i] == DIRT)
					System.out.print(" D ");
				if (world[j][i] == HOME)
					System.out.print(" H ");
			}
			System.out.println("");
		}
	}
}

class MyAgentProgram implements AgentProgram {

	private int initnialRandomActions = 10;
	private Random random_generator = new Random();

	// Here you can define your variables!

	public static final int RUN_PHASE = 1;
	public static final int START_PHASE = 0;
	public static final int HOME_PHASE = 2;
	public int currentState = 0;
	public int phase = START_PHASE;

	public int homeX;
	public int homeY;

	public int iterationCounter = 5000;
	public MyAgentState state = new MyAgentState();

	// moves the Agent to a random start position
	// uses percepts to update the Agent position - only the position, other
	// percepts are ignored
	// returns a random action
	private Action moveToRandomStartPosition(DynamicPercept percept) {
		int action = random_generator.nextInt(6);
		initnialRandomActions--;
		state.updatePosition(percept);
		if (action == 0) {
			state.agent_direction = ((state.agent_direction - 1) % 4);
			if (state.agent_direction < 0)
				state.agent_direction += 4;
			state.agent_last_action = state.ACTION_TURN_LEFT;
			return LIUVacuumEnvironment.ACTION_TURN_LEFT;
		} else if (action == 1) {
			state.agent_direction = ((state.agent_direction + 1) % 4);
			state.agent_last_action = state.ACTION_TURN_RIGHT;
			return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		}
		state.agent_last_action = state.ACTION_MOVE_FORWARD;
		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}

	private Action turnLeft() {
		state.agent_last_action = state.ACTION_TURN_LEFT;
		state.agent_direction = (state.agent_direction - 1 < 0)?3:state.agent_direction - 1;
		return LIUVacuumEnvironment.ACTION_TURN_LEFT;
	}

	private Action turnRight() {
		state.agent_last_action = state.ACTION_TURN_RIGHT;
		state.agent_direction = ((state.agent_direction + 1) % 4);
		return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
	}

	private Action moveForward() {
		state.agent_last_action = state.ACTION_MOVE_FORWARD;
		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}

	private Action suck() {
		state.agent_last_action = state.ACTION_SUCK;
		return LIUVacuumEnvironment.ACTION_SUCK;
	}

	@Override
	public Action execute(Percept percept) {

		// DO NOT REMOVE this if condition!!!
		if (initnialRandomActions > 0) {
			return moveToRandomStartPosition((DynamicPercept) percept);
		} else if (initnialRandomActions == 0) {
			// process percept for the last step of the initial random actions
			initnialRandomActions--;
			state.updatePosition((DynamicPercept) percept);
			System.out.println("Processing percepts after the last execution of moveToRandomStartPosition()");
			state.agent_last_action = state.ACTION_SUCK;
			return LIUVacuumEnvironment.ACTION_SUCK;
		}

		// This example agent program will update the internal agent state while only
		// moving forward.
		// START HERE - code below should be modified!

		System.out.println("x=" + state.agent_x_position);
		System.out.println("y=" + state.agent_y_position);
		System.out.println("dir=" + state.agent_direction);

		iterationCounter--;

		if (iterationCounter == 0)
			return NoOpAction.NO_OP;

		DynamicPercept p = (DynamicPercept) percept;
		Boolean bump = (Boolean) p.getAttribute("bump");
		Boolean dirt = (Boolean) p.getAttribute("dirt");
		Boolean home = (Boolean) p.getAttribute("home");
		System.out.println("percept: " + p);

		// State update based on the percept value and the last action
		state.updatePosition((DynamicPercept) percept);
		if (bump) {
			switch (state.agent_direction) {
			case MyAgentState.NORTH:
				state.updateWorld(state.agent_x_position, state.agent_y_position - 1, state.WALL);
				break;
			case MyAgentState.EAST:
				state.updateWorld(state.agent_x_position + 1, state.agent_y_position, state.WALL);
				break;
			case MyAgentState.SOUTH:
				state.updateWorld(state.agent_x_position, state.agent_y_position + 1, state.WALL);
				break;
			case MyAgentState.WEST:
				state.updateWorld(state.agent_x_position - 1, state.agent_y_position, state.WALL);
				break;
			}
		}
		if (dirt)
			state.updateWorld(state.agent_x_position, state.agent_y_position, state.DIRT);
		else
			state.updateWorld(state.agent_x_position, state.agent_y_position, state.CLEAR);

		state.printWorldDebug();
		
		int deltaX,deltaY;
		switch (phase) {
		case START_PHASE:// Goto north looking east
			switch (currentState) {
			case 0: // facing unknow
				System.out.println("TURN TO NORTH");
				if (state.agent_direction == MyAgentState.NORTH) {
					currentState = 1;
					return moveForward();
				} else if (state.agent_direction == MyAgentState.EAST) {
					currentState = 1;
					return turnLeft();
				} else if (state.agent_direction == MyAgentState.WEST) {
					currentState = 1;
					return turnRight();
				} else {
					currentState = 0;
					return turnLeft();
				}
			case 1:// facing north
				System.out.println("GOTO NORTH");
				if (bump) {
					currentState = 2;
					return turnLeft();
				} else {
					return moveForward();
				}
			case 2:// first line, facing east
				System.out.println("GOTO START");
				if (bump) {
					phase = RUN_PHASE;
					currentState = 0;
					return turnLeft();
				} else {
					return moveForward();
				}
			}
			break;

		case RUN_PHASE:// run the room
			System.out.println("RUN PHASE");
			if (home) {
				state.updateWorld(state.agent_x_position, state.agent_y_position, state.HOME);
				homeX = state.agent_x_position;
				homeY = state.agent_y_position;
			}
			switch (currentState) {
			case 0: // TOP LEFT - FACING SOUTH and go down
				if (dirt) {
					return suck();
				} else if (bump) {
					currentState = 1;
					return turnLeft();
				} else {
					return moveForward();
				}
			case 1: // reach the buttom, facing east
				currentState = 2;
				return moveForward();
			case 2:
				if (bump) {// reach the corner bottom, all the room has been cleaned
					phase = HOME_PHASE;
					currentState = 0;
					return turnLeft();
				}
				currentState = 3;
				return turnLeft();
			case 3: // BOTTOM - FACING NORTH and go up
				if (dirt) {
					return suck();
				} else if (bump) {
					currentState = 4;
					return turnRight();
				} else {
					return moveForward();
				}
			case 4:
				currentState = 5;
				return moveForward();
			case 5:
				if (bump) {// reach the corner bottom, all the room has been cleaned
					phase = HOME_PHASE;
					currentState = 0;
					return turnLeft();
				}
				currentState = 0;
				return turnRight();

			}
			break;

		case HOME_PHASE: // go back home
			switch (currentState) {
			case 0: // set x to home
				deltaX = homeX - state.agent_x_position;
				
				if(deltaX < 0) { // need to go east
					currentState = 1;
					return turnLeft();
				}
				
				if(deltaX > 0) { // need to go west
					currentState = 1;
					return turnRight();
				}
				
				if(deltaX == 0){
					deltaY = homeY - state.agent_y_position;
					// currently facing north
					if(deltaY == 0) {//above the home
						return NoOpAction.NO_OP;
					} else if (deltaY > 0){ // need to go south
						currentState = 1;
						return turnLeft();
					} else { // go north
						currentState = 2;
						return moveForward();
					}
				}
					
			case 1: // set x to home
				deltaX = homeX - state.agent_x_position;
				if(deltaX == 0){
					//fasing east or west
					deltaY = homeY - state.agent_y_position;
					// currently facing north
					if(deltaY == 0) {//above the home
						return NoOpAction.NO_OP;
					} else if (deltaY > 0){ // need to go south
						if(state.agent_direction == MyAgentState.EAST) {
							currentState = 2;
							return turnRight();
						} else if (state.agent_direction == MyAgentState.WEST){
							currentState = 2;
							return turnLeft();
						} else {
							System.out.println("Should never happen");
							return NoOpAction.NO_OP;
						}
					} else { // go north
						if(state.agent_direction == MyAgentState.EAST) {
							currentState = 2;
							return turnLeft();
						} else if (state.agent_direction == MyAgentState.WEST){
							currentState = 2;
							return turnRight();
						} else {
							System.out.println("Should never happen");
							return NoOpAction.NO_OP;
						}
					}
				}else {
					return moveForward();
				}
			case 2:
				deltaY = homeY - state.agent_y_position;
								
				if( deltaY == 0){
					return NoOpAction.NO_OP;
				}else {
					return moveForward();
				}
			}


		}
		// Next action selection based on the percept value
		if (dirt) {
			System.out.println("DIRT -> choosing SUCK action!");
			state.agent_last_action = state.ACTION_SUCK;
			return LIUVacuumEnvironment.ACTION_SUCK;
		} else {
			if (bump) {
				state.agent_last_action = state.ACTION_NONE;
				return NoOpAction.NO_OP;
			} else {
				state.agent_last_action = state.ACTION_MOVE_FORWARD;
				return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
			}
		}
	}
}

public class MyVacuumAgent extends AbstractAgent {
	public MyVacuumAgent() {
		//super(new MyAgentProgram());
		super(new MyVacuumProgramObstacles()); // with obs
	}
}
