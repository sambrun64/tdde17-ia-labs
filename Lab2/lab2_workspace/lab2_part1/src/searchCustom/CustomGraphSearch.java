package searchCustom;

import java.util.ArrayList;
import java.util.HashSet;

import searchShared.NodeQueue;
import searchShared.Problem;
import searchShared.SearchObject;
import searchShared.SearchNode;

import world.GridPos;

public class CustomGraphSearch implements SearchObject {

	private HashSet<SearchNode> explored; // The hash that represent all the Nodes that has been explored
	private NodeQueue frontier; // The Array that store all the node left to be treated
	protected ArrayList<SearchNode> path; // The Array that represent the path or node needed to be taken
	private boolean insertFront; // The flag that define if the frontier is FIFO or LIFO

	/**
	 * The constructor tells graph search whether it should insert nodes to front or back of the frontier 
	 */
    public CustomGraphSearch(boolean bInsertFront) {
		insertFront = bInsertFront; //true : LIFO (stack) Depth, false : FIFO (queue) Breadth
    }
    
    // Function that add a Node to the frontier depending of its type
    public void addNode(NodeQueue queue,SearchNode n){
		if(insertFront){
			queue.addNodeToFront(n);
		}else {
			queue.addNodeToBack(n);
		}
	}

    // Function that remove and return the first element if the frontier
    public SearchNode removeNode(NodeQueue queue) {
    	return queue.removeFirst();
    }
    
    /**
	 * Implements "graph search", which is the foundation of many search algorithms
	 */
	public ArrayList<SearchNode> search(Problem p) {
		// The frontier is a queue of expanded SearchNodes not processed yet
		frontier = new NodeQueue();
		/// The explored set is a set of nodes that have been processed 
		explored = new HashSet<SearchNode>();
		// The start state is given
		GridPos startState = (GridPos) p.getInitialState();
		// Initialize the frontier with the start state  
		frontier.addNodeToFront(new SearchNode(startState));

		// Path will be empty until we find the goal.
		path = new ArrayList<SearchNode>();
		
		// Implement this!
		SearchNode current;
		while(!frontier.isEmpty()){
			
			// Get the needed node depending of the algorithm
			current = removeNode(frontier);
			
			// If not already explored then skip
			if(explored.contains(current)) continue;
			
			// Add the current node to the explored state
			explored.add(current);
			
			// If the goal is reached
			if(p.isGoalState(current.getState())) {
				path = current.getPathFromRoot();
				break;
			}
			
			// Get all reachable node from the current state and treat them
			for(GridPos gp : p.getReachableStatesFrom(current.getState())) {
				SearchNode n = new SearchNode(gp, current);
				
				// We add the node only if it was never visited before
				if(!explored.contains(n)) {
					addNode(frontier, n);
				}
			}
			
		}
		
		
		/* Some hints:
		 * -Read early part of chapter 3 in the book!
		 * -You are free to change anything how you wish as long as the program runs, but some structure is given to help you.
		 * -You can Google for "javadoc <class>" if you are uncertain of what you can do with a particular Java type.
		 * 
		 * -SearchNodes are the nodes of the search tree and contains the relevant problem state, in this case x,y position (GridPos) of the agent 
		 * --You can create a new search node from a state by: SearchNode childNode = new SearchNode(childState, currentNode);
		 * --You can also extract the state by .getState() method
		 * --All search structures use search nodes, but the problem object only speaks in state, so you may need to convert between them 
		 * 
		 * -The frontier is a queue of search nodes, open this class to find out what you can do with it! 
		 * 
		 * -If you are unfamiliar with Java, the "HashSet<SearchNode>" used for the explored set means a set of SearchNode objects.
		 * --You can add nodes to the explored set, or check if it contains a node!
		 * 
		 * -To get the child states (adjacent grid positions that are not walls) of a particular search node, do: ArrayList<GridPos> childStates = p.getReachableStatesFrom(currentState);
		 * 
		 * -Depending on the addNodesToFront boolean variable, you may need to do something with the frontier... (see book)
		 * 
		 * -You can check if you have reached the goal with p.isGoalState(NodeState)
		 * 
		 *  When the goal is found, the path to be returned can be found by: path = node.getPathFromRoot();
		 */
		/* Note: Returning an empty path signals that no path exists */
		return path;
	}

	/*
	 * Functions below are just getters used externally by the program 
	 */
	public ArrayList<SearchNode> getPath() {
		return path;
	}

	public ArrayList<SearchNode> getFrontierNodes() {
		return new ArrayList<SearchNode>(frontier.toList());
	}
	public ArrayList<SearchNode> getExploredNodes() {
		return new ArrayList<SearchNode>(explored);
	}
	public ArrayList<SearchNode> getAllExpandedNodes() {
		ArrayList<SearchNode> allNodes = new ArrayList<SearchNode>();
		allNodes.addAll(getFrontierNodes());
		allNodes.addAll(getExploredNodes());
		return allNodes;
	}

}
