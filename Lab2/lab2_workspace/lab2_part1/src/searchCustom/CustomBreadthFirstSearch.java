package searchCustom;

import java.util.Random;

public class CustomBreadthFirstSearch extends CustomGraphSearch {

	public CustomBreadthFirstSearch(int maxDepth) {
		super(false); // In that case we select a QUEUE (FIFO)
	}
};
